<?php
/*Cong create 14/07/2016*/
/* Work for Beta Messenger */


require_once '../../conponent_hotel/User_DbHandler.php';
require_once '../../conponent_hotel/Gonc_String_Methold.php';
require_once '../../conponent_hotel/Hotel_DbHandler.php';

require_once '../../conponent_hotel/Room_Service_Detail_DbHandler.php';
require_once '../../conponent_hotel/Category_Room_Service_DbHandler.php';
require_once '../../conponent_hotel/Room_Service_DbHandler.php';

require_once 'Social_Tempt_Cart_DbHandler.php';


require_once '../../amazonwebservice/ConnectDynamoDB.php';
require_once '../../amazonwebservice/Config.php'; 

require '../../libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();

function authenticate(\Slim\Route $route) {
   $headers = apache_request_headers();
    $response = array();
    $app = \Slim\Slim::getInstance();    
    if (isset($headers['Authorization'])) {       
        $token_connect_server = $headers['Authorization'];        
        if ($token_connect_server != 'TokeNodejsConnectToButleric') {
          $response["error"] = true;
          $response["message"] = "Access Denied. Invalid Api key";
          echoRespnse(401, $response);
          $app->stop();       
        }
    } else {           
            $response["error"] = true;
            $response["message"] = "Authorization is misssing";
            echoRespnse(400, $response);
            $app->stop();
    }
}





$app->post('/getRoomService', 'authenticate' , function() use ($app) {  

      verifyRequiredParams(array('id_hotel'));
      $db = new ConnectDynamoDB();
      $db_Room_Service = new Room_Service_DbHandler($db);
      //$id_hotel = $app->request->post('id_hotel');

      $id_hotel = $app->request->get('id_hotel');


      $response = array();
      $iterator = $db_Room_Service->get_Room_Service_Image($id_hotel);

      if($iterator != NULL)
      {
            $response['group'] = $iterator ;
            $response['error'] = false ;
            $response['message'] = 'Found Item';  
      }
      else
      {
            $response['error'] = true;
            $response['message'] = 'No Found Item' ;
            $response['id_hotel'] = $id_hotel; 
            //$response['id_get'] = $id_get;
            
      }
     echoRespnse(201, $response);

});




$app->post('/clearTemptCart', function() use ($app) {  
    verifyRequiredParams(array('user_id'));

    $user_id = $app->request->post('user_id');

    $db_Tempt = new Social_Tempt_Cart_DbHandler();
    $clear = $db_Tempt->deleteAllItemCart($user_id);
    echoRespnse(200, $clear);
});

$app->get('/addTemptCart', function() use ($app) {  
    verifyRequiredParams(array('user_id','id'));
    echoRespnse(200, "Added");
});

$app->post('/getTemptCart', function() use ($app) {  

    // user_id is sender_id
      verifyRequiredParams(array('user_id'));


      $user_id=$app->request->post('user_id');
      
      $db = new ConnectDynamoDB();
      $db_User =new User_DbHandler($db);
      $db_CheckIn = new Check_In_DbHandler($db);
      $db_Hotel = new Hotel_DbHandler($db);
      $user = $db_User->getUserByUserID($user_id);

      if($user != null)
      {
           $user = $db_User->getUserByUserID($user_id);

                if ($user != NULL) {

                    $result = $db_CheckIn->getCheckIn($user['api_key']);
                    $response['checkin'] = $result ;
                    $response["error"] = false;
                    $response["message"] = "Logined Success !!!" ;
                    $response["user"] = $user ;
                    $response["api_key=====>"] =  $user['api_key'] ;
                    $db_Hotel_Staff = new Hotel_Staff_DbHandler($db);
                    $staff = $db_Hotel_Staff->get_Staff_From_User_Id($user_id);
                    if(count($staff['staff']) > 0){
                      $hotel = $db_Hotel->get_Hotel_From_Id_Hotel($staff['staff']['id_hotel']);
                      $response["hotels"] = $hotel['hotels'];
                    }else{
                      $hotel = $db_Hotel->get_Hotel_From_Api($user['api_key']);
                      $response["hotels"] = $hotel['hotels'];
                    }
                    echoRespnse(200, $response);
               }
               else
               {

                    $response["message"] = "Logined is failed " ;
                    $response["error"] = true ;
                    echoRespnse(200, $response);

               }

      }else
      {
                    $response["message"] = "Logined is failed " ;
                    $response["error"] = true ;
                    echoRespnse(200, $response);

      }
});


// ====================================


    
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
  
    if ($_SERVER['REQUEST_METHOD'] == 'PUT' || $_SERVER['REQUEST_METHOD'] == 'DELETE' ) {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }
  
    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
  function validateEmail($email){
     $app = \Slim\Slim::getInstance();
     if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
     }
  }

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
 
function echoRespnse($status_code, $response) {

    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
  
}

$app->run();
?>