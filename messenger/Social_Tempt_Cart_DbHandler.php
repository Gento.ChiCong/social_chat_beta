<?php

	   /*Cong Create 14/07/2016*/
	   /* Use for beta messenger order */
	 
	class Social_Tempt_Cart_DbHandler
	{
		private $conn;    
		public static $tbTemptCart = 'Social_Tempt_Cart' ;
		
		function __construct($db_) {
		     
	           $db = $db_ ;
	           $this->conn = $db->openedDynamoDB() ;
	    }
		 
		 /*
		 	type_service = 0 : room service
		 	type_service = 1 : butler service
		 */
	    public function createdTemptCart(	$sender_id,							    	
									    	$item_id,
									    	$image,
									    	$pride,
									    	$id_hotel,
									    	$type_service
									    ) {
	    	$result = array();
	    	try {	
	            $serial = self::getNewSerial();
				$this->conn->putItem(array(	'TableName' => ''.self::$tbTemptCart,
											'Item' => array('sender_id' 	=> array('S' => ''.$sender_id),
															'serial' 		=> array('N' => ''.$serial),
															'item_id' 		=> array('N' => ''.$item_id),
															'image' 		=> array('S' => ''.$image),
															'pride' 		=> array('S' => ''.$pride),
															'id_hotel' 		=> array('S' => ''.$id_hotel),
															'type_service' 	=> array('N' => ''.$type_service)
															)
										)
									);
				
				$result['error']	= false;
				$result['serial']	= $serial;

	        } catch (Exception $e) {
		       $result['error']		= true;
		       $result['messenge']	= $e;      
	        }	

			return $result  ;		
	    }


		public function getNewSerial()
		{	      
		    $item = 0 ;
			try{
				$response=$this->conn->query(array(	'TableName' => ''.self::$tbTemptCart,												
													'KeyConditions' => array(
														'sender_id' => array(
															'ComparisonOperator' => 'EQ',
															'AttributeValueList' => array (array ('S' => $sender_id))
															)
														),
													'ScanIndexForward' => false										
												));
				if (count($response['Items']) > 0) {
					$item = $response['Items'][0]['serial']['N'] + 1;
				}
				else{
					$item = 1 ;
				}		         
			} 
			catch(Exception $e){
				$item = 0 ;
			}
		    return $item ;
		}


		public function getTemptCart($api_key)
		{	
	   		$result = array();
	        try{
		        $response = $this->conn->query(array(
												'TableName' => ''.self::$tbTemptCart,												
												'KeyConditions' => array(
													'sender_id' => array(
														'ComparisonOperator' => 'EQ',
														'AttributeValueList' => array(array('S' => $sender_id))
														)
													),
												'ScanIndexForward' => false										
											));
			    foreach ($response['Items'] as $item) {
				    $tam = array();
					$tam['sender_id'] 		= $item['sender_id']['S'];
					$tam['serial'] 			= $item['serial']['N'];
					$tam['item_id']			= $item['item_id']['N'];
					$tam['image'] 			= $item['image']['S'];
					$tam['pride'] 			= $item['pride']['S'];
					$tam['id_hotel'] 		= $item['id_hotel']['S'];
					$tam['type_service'] 	= $item['type_service']['N'];					
				}
			    return $result ;			   		
		    } 
		    catch(Exception $e){		    	
		    	return null ;
	        }
	        return $result;
		}

		public function deleteItemCart($sender_id, $serial){
			$result = array();
			try{
				$this->conn->deleteItem(array(	'TableName' => ''.self::$tbTemptCart,
												'Key' => array(	'sender_id' => array('S' => $item['sender_id']['S']),
																'serial' => array('N' => $serial)
																)
												)
										);
				$result['error']	= false;
				$result['sender_id']= $sender_id;
				$result['serial']	= $serial;
			}catch(Exception $e){
				$result['error']	= true;
				$result['messenge']	= $e;
			}
			return $result;
		}


		public function deleteAllItemCart($sender_id)
		{
			$result = array();
			try{
		        $response = $this->conn->query(array(
												'TableName' => ''.self::$tbTemptCart,												
												'KeyConditions' => array(
													'sender_id' => array(
														'ComparisonOperator' => 'EQ',
														'AttributeValueList' => array(array('S' => $sender_id))
														)
													),
												'ScanIndexForward' => false										
											));
		        $count = 0;
			    foreach ($response['Items'] as $item) {				    		
					$this->conn->deleteItem(array(	'TableName' => ''.self::$tbTemptCart,
												'Key' => array(	'sender_id' => array('S' => $item['sender_id']['S']),
																'serial' => array('N' => $item['serial']['N'])
																)
												)
										);
					$count += 1;
				}
			    
				$result['error']	= false;
				$result['sender_id']= $sender_id;
				$result['count']	= $count;

		    }catch(Exception $e){		    	
		    	$result['error']	= true;
				$result['messenge']	= $e;	
	        }	        
	        return $result;
		}		

	}	 
?>